FROM python:3

RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    default-jdk \
    g++ \
    cmake \
    make \
    && rm -rf /var/lib/apt/lists/*


WORKDIR /usr/src/app
COPY tester.py .
COPY queens_sol* .
COPY tests.json .
COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

CMD ["python", "./tester.py", "--solution", "queens_sol"]
