import os
import json
import time
import sys
import argparse
import logging
import numpy as np
import subprocess as sp

LOG_FORMAT = '[%(asctime)s] %(levelname)s [%(module)s]: %(message)s'
logging.basicConfig(
    format=LOG_FORMAT,
    level=logging.DEBUG
)
_logger = logging.getLogger()
_logger.setLevel(logging.INFO)

BASEDIR = os.path.dirname(__file__)
PYTHON_EXECUTOR = 'python'
JAVA_COMPILER = 'javac'
JAVA_EXECUTOR = 'java'
CPP_COMPILER = "g++"
BREAK_ON_ERROR = False


def compile_sol(language, solution):
    if language == "python":
        return "", "", True
    elif language == "java":
        cmd = f"{JAVA_COMPILER} {solution}"
        p = sp.Popen(
            cmd.split(), stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE)
        stdout, stderr = p.communicate()
        if stderr:
            _logger.error(f"Compiling java file {solution} failed with the "
                          f"error:\n{stderr.decode()}")
            return stdout.decode(), stderr.decode(), False
        else:
            return stdout.decode(), "", True
    elif language == "cpp":
        binary = solution.split(".")[0]
        cmd = f"{CPP_COMPILER} {solution} -o {binary}"
        p = sp.Popen(
            cmd.split(), stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE)
        stdout, stderr = p.communicate()
        if stderr:
            _logger.error(f"Compiling java file {solution} failed with the "
                          f"error:\n{stderr.decode()}")
            return stdout.decode(), stderr.decode(), False
        else:
            return stdout.decode(), "", True
    else:
        _logger.error(
            f"Unidentified/ unsupported programming language {language}")
        sys.exit(1)


def validate_queens_position(chess_table):
    m, n = chess_table.shape

    for i in range(m):
        row_sum = sum(chess_table[i])
        if row_sum > 1:
            return False

    for j in range(n):
        col_sum = sum(chess_table[:, j])
        if col_sum > 1:
            return False

    for i in range(m):
        for j in range(n):
            if chess_table[i][j] == 1:
                for k in range(1, max(m, n)):
                    if i-k >= 0 and j-k >= 0 and chess_table[i-k][j-k] == 1:
                        return False
                    if i-k >= 0 and j+k < n and chess_table[i-k][j+k] == 1:
                        return False
                    if i+k < m and j-k >= 0 and chess_table[i+k][j-k] == 1:
                        return False
                    if i+k < m and j+k < n and chess_table[i+k][j+k] == 1:
                        return False

    return True


def validate_output(test_idx, stdout, expected_count, in_str):
    try:
        lines = [ln.strip() for ln in stdout.split("\n") if ln]
        count = int(lines[0])
        matrix = np.array(
            [[int(i.strip()) for i in ln.split(" ")] for ln in lines[1:]])
        [m, n] = [int(i) for i in in_str.split(' ')]
        if count != expected_count:
            _logger.error(
                f"Mismatch! Expecting {expected_count} queens. "
                f"Obtained {count} queens")
            return count, False
        elif (m, n) != matrix.shape:
            _logger.error(f"The chess table doesn't respect the shape.\nReal "
                          f"(m, n): ({m}, {n})\nConfiguration (m, n): "
                          f"{matrix.shape}")
            return count, False
        return count, validate_queens_position(matrix)
    except Exception as e:
        _logger.error(f'Invalid stdout for test {test_idx}')
        return None, False


def run_test(test_idx, test_config, language, solution):
    in_str = test_config['input']
    timeout = test_config['timeout']
    if language == 'python':
        cmd = f"{PYTHON_EXECUTOR} {solution}"
    elif language == 'java':
        class_name = solution.split('.')[0]
        cmd = f"{JAVA_EXECUTOR} {class_name}"
    elif language == 'cpp':
        binary = solution.split(".")[0]
        cmd = f"./{binary}"
    else:
        _logger.error(
            f"Unidentified/ unsupported programming language {language}")
        sys.exit(1)

    t1 = time.time()
    p = sp.Popen(cmd.split(), stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE)
    try:
        stdout, stderr = p.communicate(input=in_str.encode(), timeout=timeout)
    except Exception as e:
        _logger.error(f"Test failed due to timeout:\n{e}")
        stderr = str(e).encode()
        stdout = "".encode()
    t2 = time.time()
    if stderr:
        _logger.error(
            f"Test {test_idx} execution failed with the error\n"
            f"{stderr.decode()}")
        count, valid_result = None, False
    else:
        count, valid_result = validate_output(
            test_idx, stdout.decode(), test_config['expected_count'],
            test_config['input'])
        if valid_result:
            _logger.info(f"Test {test_idx} PASSED after {round(t2-t1, 2)} "
                         f"seconds of execution")
        else:
            _logger.error(f"Test {test_idx} FAILED after {round(t2-t1, 2)} "
                          f"seconds of execution")
    return {
        "test_idx": test_idx,
        "nof_queens": count,
        "valid_result": valid_result,
        "duration": t2-t1,
        "input_config": test_config,
        "stdout": stdout.decode(),
        "stderr": stderr.decode()
    }


if __name__ == "__main__":
    parser = argparse.ArgumentParser(__doc__)
    parser.add_argument(
        "--test_file", default=os.path.join(BASEDIR, "tests.json"),
        help="The JSON file containing an array of all the input tests")
    parser.add_argument(
        "--solution", required=True,
        help="Path to the solution file which is going to be tested")

    args = parser.parse_args()
    solutions = [i for i in os.listdir(BASEDIR) if args.solution in i]
    if not solutions:
        _logger.error(f"No file with the name {args.solution} was found in "
                      f"the tester's directory")
        sys.exit(1)
    elif solutions[0] == f"{args.solution}.py":
        language = 'python'
        args.solution = f"{args.solution}.py"
    elif solutions[0] == f"{args.solution}.java":
        language = 'java'
        args.solution = f"{args.solution}.java"
    elif solutions[0] == f"{args.solution}.cpp":
        language = 'cpp'
        args.solution = f"{args.solution}.cpp"
    else:
        _logger.error(f"Invalid extension for {solutions[0]}")
        sys.exit(1)
    _logger.info(f"Identified programming language: {language}")

    with open(os.path.expandvars(args.test_file)) as f:
        tests = json.load(f)

    report = []
    for ti, test_config in enumerate(tests):
        test_idx = ti + 1
        _logger.info(f"Compiling test {test_idx}...")
        stdout, stderr, successful_compile = compile_sol(
            language, args.solution)
        if not successful_compile:
            test_report = {
                "test_idx": test_idx,
                "nof_queens": None,
                "valid_result": False,
                "duration": 0,
                "input_config": test_config,
                "stdout": stdout,
                "stderr": stderr
            }
        _logger.info(
            f"Running test {test_idx}: Inputs: {test_config['input']}...")
        test_report = run_test(
            test_idx, test_config, language, args.solution)
        report.append(test_report)
        if not test_report['valid_result'] and BREAK_ON_ERROR:
            break

    _logger.info(f"The detail report of the solution can be seen bellow:\n"
                 f"{json.dumps(report, indent=4)}")

    score = len([i for i in report if i['valid_result']])
    nof_tests = len(report)
    status = 'PASSED' if score == nof_tests else 'FAILED'
    if score == nof_tests:
        _logger.info(f'SUCCESS! {score} out of {nof_tests} tests PASSED')
        sys.exit(0)
    else:
        _logger.error(f'FAILURE! {score} out of {nof_tests} tests PASSED')
        sys.exit(1)
