#include "stdio.h"
#include "stdlib.h"


class chessTable {
    int m, n;
    char *rows, *cols, *mdiags, *sdiags;
    char **all;
    int count = 0;
public:
    chessTable(int m, int n) {
        this->m = m;
        this->n = n;
        all = (char**) malloc(m*sizeof(char*));
        for (int i = 0; i < m; i++)
            all[i] = (char*) calloc(n, sizeof(char));
        
        rows = (char*)calloc(m, sizeof(char));
        cols = (char*)calloc(n, sizeof(char));
        mdiags = (char*)calloc(m+n-1, sizeof(char));
        sdiags = (char*)calloc(m+n-1, sizeof(char));

    }
    int mdindex(int i, int j) {
        return j-i+m-1;
    }
    int sdindex(int i, int j) {
        return (n-1-j)-i+m-1;
    }

    int checkPiece(int i, int j) {
        return rows[i] || cols[j] || mdiags[mdindex(i,j)] || sdiags[sdindex(i,j)] || i >= m || i < 0 || j >= n || j < 0;
    }

    void placePiece(int i, int j) {
        rows[i] = 1;
        cols[j] = 1;
        mdiags[mdindex(i, j)] = 1;
        sdiags[sdindex(i, j)] = 1;
        all[i][j] = 1;
        count++;
    }

    void removePiece(int i, int j) {
        rows[i] = 0;
        cols[j] = 0;
        mdiags[mdindex(i, j)] = 0;
        sdiags[sdindex(i, j)] = 0;
        all[i][j] = 0;
        count--;
    }

    void printAll(){
        for (int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++)
                printf("%d ", all[i][j]);
            printf("\n");
        }   
    }

    int tblcpy(const chessTable& src) {
        if(src.m!=m || src.n != n)
            return 0;
        count = src.count;
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                all[i][j]=src.all[i][j];
        for (int i = 0; i < n; i++)
            rows[i] = src.rows[i];
        for (int i = 0; i < m; i++)
            rows[i] = src.cols[i];
        for (int i = 0; i < n+m-1; i++)
            rows[i] = src.mdiags[i];
        for (int i = 0; i < n+m-1; i++)
            rows[i] = src.sdiags[i];
        return 1;
    } 

    int getCount() {
        return count;
    }

    const char* getRows() const { return rows; }
    const char* getCols() const { return cols; }
    const char* getMDiags() const { return mdiags; }
    const char* getSDiags() const { return sdiags; }
    int getRowNumber() const { return m; }
    int getColNumber() const { return n; }
};

/*
1 0 0 0 0 0 0 0
0 0 1 0 0 0 0 1
0 0 0 0 0 0 0 0
0 1 0 0 0 0 0 0
0 0 0 0 0 0 1 0
0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0
0 0 0 1 0 0 0 0

*/

int back1(chessTable& table, chessTable& maxtable, int k) {
    static int kmax = 0;
    if(k > kmax) {
        maxtable.tblcpy(table);
        kmax = k;
    }
    if(k >= table.getRowNumber()){ kmax = 0; return 1; } 
    for(int i = 0; i < table.getColNumber(); i++){
        if(!table.checkPiece(k, i)){
            table.placePiece(k, i);
            if(back1(table, maxtable, k+1)) return 1;
            table.removePiece(k, i);
        }
    }
    return 0;
}

int back2(chessTable& table, chessTable& maxtable, int k) {
    static int kmax = 0;
    if(k > kmax) {
        maxtable.tblcpy(table);
        kmax = k;
    }
    if(k >= table.getColNumber()) {
        kmax = 0;
        return 1;
    }
    for(int i = 0; i < table.getRowNumber(); i++){
        if(!table.checkPiece(i, k)){
            table.placePiece(i, k);
            if(back2(table, maxtable, k+1)) return 1;
            table.removePiece(i, k);
        }
    }
    return 0;
}

int main() {
    int m, n;
    scanf("%d%d", &m, &n);
    chessTable table(m,n), maxtable(m,n);
    if(m > n)
        back2(table, maxtable, 0);
    else
        back1(table, maxtable, 0);
    printf("%d\n", maxtable.getCount());
    maxtable.printAll();
    // Marcam liniile si coloanele deja luate, cat si diagonalele

    // vor fi diagonale cate linii+cate coloane-1 care e diagonala principala luata de 2 ori
    // intai vor fi cele m linii, asa ca la indexare se va aduna m-1 (pentru a lua diagonala principala 0)

    // indexarea diagonalei va fi j-i+m-1 la diagonalele principale
    // j-(n-i-1)+m-1 la cele secundare

    

    /*
    greedy:
        pentru a nu se putea ataca reginele ar trebui sa fie in o pozitie care nu e pe diagonala/linie
        In acelasi timp trebuie impachetate in asa fel incat sa fie cat mai multe
        => pozitia de cal e destul de buna
    */

    // printf("%d", m > n ? n : m); // a slight misunderstanding, oops
    return 0;
}

